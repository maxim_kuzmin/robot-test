  BASIC_COMMANDS  = %w[move left right report].freeze
class Robot
  BASIC_COMMANDS_REGEXPS = BASIC_COMMANDS.map{ |c| /^\s*#{Regexp.quote(c)}\s*$/i}

  PLACE_REGEXP = /^\s*PLACE\s*\d[,]\s*\d\s*[,]\s*(north|west|south|east)\s*$/i

  COMMAND_REGEXPS = BASIC_COMMANDS_REGEXPS.push PLACE_REGEXP

  def initialize(commands: nil)
    @commands = commands.nil? ? [] : commands.split("\n")
  end

  def start
    @commands.any? ? execute_commands : start_robot_repl
  end

  private

  def start_robot_repl
    @index = 0
    loop do
      line = STDIN.gets.chomp
      break if line.match(/^exit$/i)
      interprete_line line, index
      index += 1
    end
  end

  def execute_commands
    @commands.each_with_index{ |line, index| interprete_line(line, index) }
  end

  def interprete_line(line, index = nil)
    @line, @index = line.downcase, index
    if line_valid?
      method = @line.scan(/^[*\w]*\s/).first
      args =
    else
      report_wrong_line
    end
  end

  def line_valid?
    is_valid = false
    COMMAND_REGEXPS.each do |regexp|
      is_valid = !!@line.match(regexp)
      break if is_valid
    end
    is_valid
  end

  def move
    puts 'move'
  end

  def left
    puts 'left'
  end

  def right
    puts "right"
  end

  def place(x, y, direction)
    puts "place #{[x,y, direction]}"
  end
end

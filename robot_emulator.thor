require 'thor'
require './app/robot'

class RobotEmulator < Thor
  desc 'start', 'starts robot REPL'
  def start
    Robot.new.start
  end

  desc 'start_from FILE', 'reads commands from file'
  def start_from(file)
    opened_file = File.open(file)
    commands = opened_file.read
    opened_file.close
    Robot.new(commands: commands).start
  end
end

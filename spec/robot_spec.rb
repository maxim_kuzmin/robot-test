require './app/robot'

RSpec.describe Robot do
  context 'basic class interfaces' do
    let(:commands_file){File.read('spec/fixtures/commands.txt')}

    it 'should respond to start command' do
      expect(Robot.new.respond_to? :start).to be(true)
    end

    context 'repl' do
      let(:robot){ Robot.new }
      it 'should start_robot_repl if commands not received ' do
        expect(robot).to receive(:start_robot_repl)
        robot.start
      end
    end

    context 'from_file' do
      let(:robot){ Robot.new(commands: commands_file) }
      it 'should start_robot_repl if commands not received ' do
        expect(robot).to receive(:execute_commands)
        robot.start
      end
    end

    context '#interprete_line' do
      let(:base_commands){
        "place 1,2, north \n"
      }
      let(:robot){|commands| Robot.new(commands: base_commands) }

      it 'should read place command' do
        robot = Robot.new(commands: base_commands)
        expect(robot).to receive(:place).with(1, 2, 'north')
        robot.start
      end
    end
  end
end
